#!/bin/bash

# ALL FIELDS MUST BE COMPLETED
DATABASE_GIT_PATH="/home/cmoldovan/public_html/modensie/Database"
DATABASE_SOURCE_PATH="/home/cmoldovan/public_html/modensie/Database"
DB_SERVER_USER=""
DB_SERVER_PASS=""
BACKEND_GIT_PATH="/home/cmoldovan/public_html/modensie/Backend"
BACKEND_SOURCE_PATH="/home/cmoldovan/public_html/modensie/Backend/Modensie"
FRONTEND_GIT_PATH="/home/cmoldovan/public_html/modensie/Frontend"
FRONTEND_SOURCE_PATH="/home/cmoldovan/public_html/modensie/Frontend/WebModensie"
		
# START OF THE AUTOMATION
echo "Check Database server..."
cd $DATABASE_GIT_PATH
if [[ $(git ls-remote origin refs/heads/master) == *$(git rev-parse HEAD)* ]];
then
	echo "Database data is up to date!"
else
	echo "There's a newer version on Git! Performing a pull request from git..."
	git pull origin master &
	wait $!
	echo "Database is now up to date! Updating the database Tables..."
	cd $DATABASE_SOURCE_PATH
	mysql -u ${DB_SERVER_USER} --password=${DB_SERVER_PASS} <<< "source database_modensie_initialization.sql;" &
	wait $!
	echo "Populate the database Tables..."
	mysql -u ${DB_SERVER_USER} --password=${DB_SERVER_PASS} <<< "source database_modensie_registrations_opt.sql;" &
	wait $!
	echo "Database was successfully updated!"
fi

echo "Check Tomcat server..."
cd $BACKEND_GIT_PATH
if [[ $(git ls-remote origin refs/heads/master) == *$(git rev-parse HEAD)* ]];
then
        echo "Backend data is up to date!"
else
        echo "There's a newer version on Git! Stopping the Backend server..."
        fuser -k 8080/tcp
        echo "Performing a pull request from git..."
        git pull origin master &
        wait $! ## Wait for the last process
        echo "Backend is now up to date! The Tomcat server will start soon!"
        cd $BACKEND_SOURCE_PATH
        mvn spring-boot:run -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses &
        BACKEND_SERVER_PID=$!
		sleep 1m
fi

echo "Check Frontend server..."
cd $FRONTEND_GIT_PATH
if [[ $(git ls-remote origin refs/heads/master) == *$(git rev-parse HEAD)* ]];
then
        echo "Frontend data is up to date!"
else
        echo "There's a newer version on Git! Performing a pull request from git..."
        git pull origin master &
        wait $!
        echo "Frontend is now up to date!"
fi

echo "Checking the status of BACKEND and FRONTEND servers..."
if [[ $(ss -tulpn | grep ':8080') ]];
then
        echo "Backend Server is LIVE!"
else
        echo "Backend Server is NOT RESPONDING! Starting the server again..."
        #kill -9 $BACKEND_SERVER_PID #Stop process if was already started
        cd $BACKEND_SOURCE_PATH
        mvn spring-boot:run -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses &
        BACKEND_SERVER_PID=$!
fi

if [[ $(ss -tulpn | grep ':4200') ]];
then
        echo "Frontend Server is LIVE!"
else
        echo "Frontend Server is NOT RESPONDING! Starting the server again..."
        cd $FRONTEND_SOURCE_PATH
        ng serve --host 0.0.0.0 --disable-host-check &
        FRONTEND_SERVER_PID=$!
fi

wait $BACKEND_SERVER_PID ## Wait for backend server
wait $FRONTEND_SERVER_PID ## Wait for frontend server

echo "Done"
